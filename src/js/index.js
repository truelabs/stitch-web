(function(window, document) {
    'use strict'

    var StitchAPI = window.StitchAPI
    var currentEventId
    var currentEvent

    function imageScalingFactor() {
        var mm = window.matchMedia
        if (!mm) {
            console.warning("Browser doesn't support window.matchMedia, using scale factor of 1.")
            return 1
        }
        var i
        for (i = 4; i > 1; --i) {
            var query = "(min-resolution: " + i + "dppx)"
            if (mm(query).matches) {
                return i
            }
        }
        return i
    }

    // Create CSS rules to handle the sizes of all the captures.
    // Returns { classNames, rules }, where selectors is an array
    // of CSS where classNames[i] is meant for captures[i] and
    // rules is a string of CSS rules meant to be inserted into the DOM
    // in a style element.
    function cssRulesAndClassNamesForCaptures(captures) {
        var cssSpecs = _.chain(captures).map(function(c) {
            return _.max(c.thumbnails, function(t) { return t.width })
        }).filter(function(t) {
            return t && t.width && t.height 
        }).map(function(t) {
            return {w: t.width, h: t.height, className: "capture-sizer-" + t.width + "x" + t.height}
        }).value();

        // For resizing, we want the image to fill up one dimension of the screen and
        // then fill the rest of the screen while maintaining aspect ratio.
        //
        // ai = aspect ratio of the image, ai = wi/hi, where wi and hi are
        // the width and height of the images.
        // as = aspect ratio of the screen.
        //
        // Given these ratios and vh (viewport height) and vw (viewport width), we can
        // compute a size that fills one dimention of the screen while maintaining
        // aspect ratio with the following rules:
        // 
        // If as == ai, then w=vw,h=vh
        // If as > ai, then w=ai*vh,h=vh
        // If as < ai, then w=vw,h=(1/ai)vw
        //
        // Which can be reduced into two inequalities by combining the first case with another:
        // If as >= ai, then w=ai*vh,h=vh
        // If as < ai, then w=vw,h=(1/ai)vw
        //
        // Note: in the above equations, vw is taken to be 100vw and vh is 100vh to simplify the expressions.
        var ruleTemplate = _.template(
            // If as < ai, then w=vw,h=(1/ai)vw
            '.<%=className%> {' +
                //'background-color: red;' +
                'width: 100vw;' +
                'height: <%=100/aspect_ratio_image%>vw;' +
            '}' +
            // If as >= ai, then w=ai*vh,h=vh
            '@media screen and (min-aspect-ratio: <%=width%>/<%=height%>) {' +
                '.<%=className%> {' +
                    //'background-color: green;' +
                    'width: <%=100*aspect_ratio_image%>vh;' +
                    'height: 100vh;' +
                '}' +
            '}'
        )

        var rules = _.chain(cssSpecs).uniq(false, function(e) {
            return e.className
        }).map(function(e) {
            return ruleTemplate({aspect_ratio_image: e.w/e.h, width: e.w, height: e.h, className: e.className})
        }).reduce(function(memo, css) {
            return memo + css
        }, "").value()

        var classNames = _.map(cssSpecs, function(s) { return s.className })
        return {classNames: classNames, rules: rules}
    }

    function loadEvent(eventResponse) {
        var event = eventResponse.event
        addRecentEvent(event.id, event.name)
        document.title = "Stitch: " + event.name
        currentEvent = event
        var name = event.name
        var captures = eventResponse.captures
        var captureLength = captures.length

        var scale = imageScalingFactor()
        var height = Math.min($(window).height(), 800) * scale
        var width = Math.min(window.screen.availWidth, 800) * scale

        var rulesAndSelectors = cssRulesAndClassNamesForCaptures(captures)
        var classNames = rulesAndSelectors.classNames
    
        var styleId = "#generated-capture-styles"
        var style = $(styleId)
        if (style.length === 0) {
            style = $('<style id="' + styleId + '"></style>')
            $("head").append(style)
        }
        style.text(rulesAndSelectors.rules)

        var captureTemplate = _.template(
                '<div class="capture" data-md5="<%= contentMD5 %>">' + 
                '<div class="captionbar">' +
                  '<div class="caption">Shared by <%- owner %></div>' +
                  '<div class="action"><img class="action-image" src="images/action-ellipses.svg"></div>' +
                '</div>' +
                '<%= item %>' +
                '</div>'
                )

        var imageTemplate = _.template(
                '<% var srcset = _.map(thumbnails, function(t) { return t.url + " " + t.width + "w"; }).join(", "); %>' +
                '<img class="lazyload capture-image <%=className%>" data-src="<%= src %>" data-sizes="auto" data-srcset="<%= srcset %>">'
                )

        var videoTemplate = _.template(
                '<video class="lazyload capture-video <%=className%>" controls preload="none" data-poster="<%= dataPoster %>">' +
                '<source src="<%= videoURL %>" type="<%= contentType %>">' +
                'Your browser does not support HTML5 video.' +
                '</video>'
                )

        var newElements = []

        for (var i = 0; i < captureLength; ++i) {
            var capture = captures[i]
            var item
            var thumbnailVariant = StitchAPI.bestThumbnailForWidthAndHeight(capture, width, height)
            var className = classNames[i]

            if (capture.content_type.indexOf("video/") === 0) {
                var videoVariant = StitchAPI.bestVideoForWidthAndHeight(capture, width, height)
                item = videoTemplate({
                    dataPoster: thumbnailVariant.url,
                    contentType: videoVariant.content_type,
                    videoURL: videoVariant.url,
                    className: className
                })
            } else if (capture.content_type.indexOf("image/") === 0) {
                item = imageTemplate({
                    thumbnails: capture.thumbnails,
                    src: thumbnailVariant.url,
                    className: className
                })
            } else {
                console.warn("Ignoring unexpected content type " + capture.content_type)
                continue
            }

            var newElement = captureTemplate({owner: capture.owner_name, item: item, contentMD5: capture.content_md5})
            newElements.push($(newElement).data('capture', capture))
        }

        $("#captures").empty().append(newElements)

        // Add event name in header
        $("#event-title").text(name)

        var $addText = $(".add-photos-text")
        var $addButton = $(".add-photos-button")
        if (StitchAPI.canPostToEvent(deviceIdentity().id, event)) {
            $addText.show()
            $addButton.removeClass("invisible")
        } else {
            $addText.hide()
            $addButton.addClass("invisibile")
        }

        updateNoPhotosAlert()
    }

    function updateNoPhotosAlert() {
        var $noPhotos = $("#no-photos-alert")
        if ($("#captures").children().length === 0) {
            $noPhotos.show()
        } else {
            $noPhotos.hide()
        }
    }

    function fetchEventCaptures(eventId) {
        return StitchAPI.getEvent(eventId).then(function(event) {
            event.captures.sort(function(a,b) {
                if (a.time > b.time) {
                    return -1
                } else if (a.time < b.time) {
                    return 1
                } else {
                    return 0
                }
            })

            return event
        })
    }

    function getURLParameter(Param){
        var PageURL = window.location.search.substring(1)
        var URLVariables = PageURL.split('&')
        for (var i = 0; i < URLVariables.length; i++){
            var ParameterName = URLVariables[i].split('=')
            if (ParameterName[0] == Param){
                return ParameterName[1]
            }
        }
    }

    function processURLParams() {

        // Is this an event link?
        var eventId = getURLParameter('event')
        if (eventId) {
            currentEventId = eventId
            console.debug("Event URL.")
            return fetchEventCaptures(eventId).then(loadEvent).then(function() {
                $("#open-in-app").prop("href", "com.funsocialapps.stitch://event/" + eventId);
                showView("event-view")
            })
        }

        // Is this an event+asset link?
        var eid = getURLParameter('eid')
        var cmd = getURLParameter('cmd')
        if (eid && cmd) {
            currentEventId = eid
            console.debug("Event capture URL.")
            return fetchEventCaptures(eid).then(function(event) {
                // If captureId is set, only match that one capture.
               event.captures = _.filter(event.captures, function(c) { return c.content_md5 == cmd; })
               return event
            }).then(loadEvent).then(function() {
                $("#open-in-app").prop("href", "com.funsocialapps.stitch://event/" + eventId);
                showView("event-view")
            })
        }
        // Add first event if event list never populated
        addFirstEvent()
        // Show event list
        loadEventList()
        showView("event-list-view")
        return $.Deferred().resolve()
    }

    function recentEventList() {
        var list = []
        try {
            var e = JSON.parse(localStorage.getItem("event-list"))
            if (_.isArray(e)) {
                list = e
            }
        } catch(exception) {
            console.error("Error parsing event list array.", exception)
        }
        return list
    }

    function saveEventList(list) {
        localStorage.setItem("event-list", JSON.stringify(list))
    }

    function addRecentEvent(id, name) {
        var l = recentEventList()
        var e = _.findWhere(l, {id: id})
        if (e) {
            e.name = name 
        } else {
            l.unshift({id: id, name: name})
        }
        saveEventList(l)
    }

    function removeRecentEvent(id) {
        var l = _.reject(recentEventList(), function(e) {
            return e.id === id
        })
        saveEventList(l)
        return l
    }

    function addFirstEvent() {
        var installed = window.localStorage.getItem("installed")
        var checkEventList = recentEventList()
        if (installed || checkEventList.length > 0) {
            console.log("Event list had event previously")
        } else {
            console.log("Adding event for first time...")
            addRecentEvent('eb42a085-6aa8-4d92-8426-342dd0066436', 'Italy Trip')
        }
        window.localStorage.setItem("installed", 1)
    }

    function loadEventList() {
        var t = _.template(
            '<button type="button" class="event-list-item list-group-item" data-eventid="<%=id%>">' +
                '<div class="event-list-item-title"><%-name%></div>' +
                '<div class="event-list-item-delete"><a class="delete-event-button">leave</a></div>' +
            '</button>')
        var list = recentEventList()
        var listItems = ""
        for (var i = 0, len = list.length; i < len; ++i) {
            var e = list[i]
            listItems += t(e)
        }

        var $noEvents = $("#no-events-alert")
        if (list.length === 0) {
            $noEvents.show()
        } else {
            $noEvents.hide()
        }

        $("#event-list").html(listItems)
    }

    function registerSetUsernameHandlers() {
        var modal = $("#change-username-modal")

        $("#username-form").submit(function(e) {
            e.preventDefault()

            var defer = modal.data("defer")

            var username = $("#username").val()
            if (!_.isString(username) || username.length === 0) {
                return defer.reject()
            }

            window.localStorage.setItem("username", username)
            window.localStorage.removeItem(deviceRegisteredKey) // force device registration
            defer.resolve(username)
            modal.modal('hide')
        })

        modal.on("hide.bs.modal", function(e) {
            modal.data("defer").reject()
        }).on("shown.bs.modal", function(e) {
            $("#username").focus()
        })
    }

    function isUsernameSet() {
        var username = window.localStorage.getItem("username")
        return _.isString(username) && username.length > 0
    }

    function promptForUsernameIfNotSet() {
        if (isUsernameSet()) {
            return $.Deferred().resolve()
        }

        return promptForUsername()
    }

    function promptForUsername() {
        var existingUsername = localStorage.getItem("username")
        if (!_.isString(existingUsername)) {
            existingUsername = ""
        }

        $("#username").val(existingUsername)

        var defer = $.Deferred()
        $("#change-username-modal").data("defer", defer).modal('show')
        return defer.promise()
    }

    function registerCreateEventHandlers() {
        var $ef = $("#event-form")
        var $submit = $ef.find(":submit")

        $("#event-modal").on("show.bs.modal", function(e) {
            $("#event-name").val("").parent().removeClass("has-warning")
            $("#event-modal-error").hide()
            $("#event-modal-success").hide()
            $submit.prop("disabled", false)
            $submit.html("Save")
        }).on("shown.bs.modal", function(e) {
            $("#event-name").focus()
        })

        $ef.submit(function(e) {
            e.preventDefault()

            console.log("saving...")
            var name = $("#event-name").val()
            if (name.trim().length === 0) {
                console.log("name is 0 length")
                $("#event-name").parent().addClass("has-warning")
                $("#event-name-small").show().text("Event name cannot be blank.")
                return
            }

            var anyoneCanPost = $("#anyone-can-post").prop("checked")

            $submit.prop("disabled", true)
            $submit.html("Saving &hellip;")

            var id = uuid.v4()
            var device = deviceIdentity()
            StitchAPI.putEvent(id, name, device.id, anyoneCanPost).done(function() {
                addRecentEvent(id, name)
                loadEventList()
                $submit.html("Saved")
                var url = "/?event=" + encodeURIComponent(id)
                var s = $("#event-modal-success")
                s.find("span").text(name)
                s.find("a").prop("href", url)
                s.show(150)
                $("#event-modal-error").hide(150)
            }).fail(function() {
                console.error("Failed to create event")
                $("#event-modal-error").show(150)
                $submit.prop("disabled", false)
                $submit.html("Save")
            })
        })
    }

    function createEvent() {
        $("#event-modal").modal("show")
    }

    function deviceIdentity() {
        var localStorage = window.localStorage
        var id = localStorage.getItem("device-id")
        var password = localStorage.getItem("device-password")

        if (!id|| !password) {
            console.debug("Generating new device ID and password.")
            id = uuid.v4()
            password = uuid.v4()
            localStorage.setItem("device-id", id)
            localStorage.setItem("device-password", password)
        }

        return { id: id, password: password }
    }

    // Get the device id and password for Stitch API. Only register the device if needed.
    var deviceRegisteredKey = "isDeviceRegistered"
    function onNeedsCredentials() {
        var device = deviceIdentity()

        if (_.isString(window.localStorage.getItem(deviceRegisteredKey))) {
            console.debug("device already registered")
            return $.Deferred().resolve(device.id, device.password)
        }

        console.debug('registering new device')
        return registerDevice()
    }

    // Register the device unconditionally. Don't prompt for a username.
    function registerDevice() {
        var device = deviceIdentity()

        var username = window.localStorage.getItem("username") || ""
        var defer = $.Deferred()

        console.debug('registering device with username', username)
        StitchAPI.registerDevice(device.id, device.password, username).done(function() {
            window.localStorage.setItem(deviceRegisteredKey, "true")
            defer.resolve(device.id, device.password)
        }).fail(function() {
            defer.reject()
        })

        return defer.promise()
    }

    var lookForCaptureDelay = 5000
    var capturesInProcessing = []
    var isLookingForCaptures = false
    var lookForCaptureAfterDelay
    function addCaptureInProcessing(eventId, deviceId, md5) {
        console.debug("monitoring " + eventId + "/" + deviceId + "/" + md5)
        var defer = $.Deferred()
        capturesInProcessing.push({eventId: eventId, owner: deviceId, content_md5: md5, defer: defer})
        if (!isLookingForCaptures) {
            isLookingForCaptures = true
            lookForCaptureAfterDelay()
        }
        return defer.promise()
    }

    function lookForCaptures() {
        console.debug('lookForCaptures')
        fetchEventCaptures(currentEventId).done(function(event) {
            var eventCaps = event.captures
            var parts = _.partition(capturesInProcessing, function(cap) {
                return _.find(eventCaps, function(c) {
                    return c.content_md5 == cap.content_md5 && c.owner == cap.owner
                })
            })
            var noLongerProcessing = parts[0], stillProgressing = parts[1]
            if (noLongerProcessing.length > 0) {
                console.debug("capture removed from processing, loading event.")
                _.each(noLongerProcessing, function(cap) {
                    cap.defer.resolve()
                })
                loadEvent(event)
            } else {
                console.debug("Nothing changed")
            }
            capturesInProcessing = stillProgressing
            if (capturesInProcessing.length > 0) {
                console.debug("Still " + capturesInProcessing.length + " captures in processing, will refresh event again.")
                lookForCaptureAfterDelay()
            } else {
                isLookingForCaptures = false
            }
        }).fail(function() {
            console.error("getEvent failed while looking for captures. Will try again.")
            lookForCaptureAfterDelay()
        })
    }

    lookForCaptureAfterDelay = function() {
        window.setTimeout(lookForCaptures, lookForCaptureDelay)
    }

    function main() {
        StitchAPI.onNeedsCredentials = onNeedsCredentials

        var badge;
        if (iOS()) {
            badge = 
                '<a href="https://itunes.apple.com/app/id990828095" target="itunes_store">' +
                '<img alt="Download on the App Store" src="images/app_store_badge_us-uk_135x40.svg">' +
                '</a>'

            $("#open-in-app").show()
        }  /* disabled until android app available
              else if (os == "android") {
            badge = 
                '<a href="https://play.google.com/store/apps/details?id=com.funsocialapps.stitch">' +
                '<img alt="Get it on Google Play" src="https://developer.android.com/images/brand/en_generic_rgb_wo_45.png">' +
                '</a>'
        }
        */


        var ads = $("#app-advertisements")
        var ads2 = $("#app-advertisements-list")

        if (badge) {
            ads.append(badge)
            ads2.append(badge)
        } else {
            ads.hide()
        }

        registerEventHandlers()
        
        processURLParams().done(function() {
            console.debug("done processing params")
            browserCompatibilityCheck()
        }).fail(function() {
            console.error("Failed to load event.")
            showView("load-fail-view")
            addAlert("Failed to load event.", "Please check the page URL and refresh the page.")
        })

        setupBranchMetrics(currentEventId)
    }

    function browserCompatibilityCheck() {
        var ok = true
        var required = ["filereader", "flexbox", "video"]
        _.each(required, function(feature) {
            if (!Modernizr[feature]) {
                ok = false
                console.log("Browser lacks required feature: " + feature)
            } else {
                console.debug("Browser has required feature: " + feature)
            }
        })

        if (!ok) {
            addAlertNoEscape("Your browser is not supported.",
                'Please download <a href="https://www.mozilla.org/firefox">Mozilla Firefox</a>' +
                ' or <a href="https://www.google.com/chrome/browser">Google Chrome</a> and try again.')
        }
    }

    // Show the view specified by viewId and hide the others.
    var currentViewId
    function showView(viewId) {
        $(".view").hide()
        $("#" + viewId).show()
        currentViewId = viewId
    }

    function uploadFilesHandler(files) {
        var uploadCardTemplate= _.template(
                '<div class="card">' +
                '<div class="left"></div>' +
                '<div class="middle"><div class="status"></div><progress class="upload-progress" value="0" max="1"></progress></div>' +
                '<div class="right"><button class="upload-cancel btn-primary">Cancel</button></div>' +
                '</div>')
        var eventId = currentEventId

        files = _.sortBy(files, "lastModifiedDate")

        promptForUsernameIfNotSet().done(function() {
            var device = deviceIdentity()
            var deviceId = device.id
            console.debug("Got device id " + deviceId)
            _.each(files, function(file) {
                console.debug("Uploading file " + file)

                var isCancelled = false

                var uploadCard = $.parseHTML(uploadCardTemplate({filename: file.name}))
                $("#uploads").append(uploadCard)
                var progress = $(uploadCard).find("progress")[0]
                var status = $(uploadCard).find(".status")[0]

                $(uploadCard).find("button").on("click", function(event) {
                    isCancelled = true
                    $(uploadCard).remove()
                })

                if (file.size > (450*1000000)) {
                    $(status).html("Failed to upload " + file.name + ". File exceeded 450MB limit.")
                    return
                }

                if (file.type.indexOf("image/") === 0) {
                    var img = document.createElement("img")
                    var src = window.URL.createObjectURL(file)
                    console.debug("Using object URL src = " + src)
                    img.src = src
                    img.onload = function() {
                        window.URL.revokeObjectURL(src)
                    }

                    var e = $(uploadCard).find(".left")

                    EXIFOrientation.cssTransformFromFile(file).done(function(cssTransform) {
                        // Since setting the CSS width and height won't take the transform into
                        // account, scale up the image so it (usually) fills out the space.
                        img.style.transform = "scale(1.5,1.5) " + cssTransform
                        e.append(img)
                    }).fail(function() {
                        img.style.transform = "scale(1.5,1.5)"
                        e.append(img)
                    })
                } else if (file.type.indexOf("video/") === 0) {
                    // TOOD: video thumbnail
                } else {
                    console.warn("No thumbnail for unhandled file type " + file.type)
                }

                $(status).html("Preparing to Upload")
                StitchAPI.uploadFile(file, eventId, deviceId).progress(function(sent, total, underlyingXHR) {
                    $(status).html("Uploading")
                    progress.value = sent/total
                    if (isCancelled) {
                        console.debug("Aborting underlying XHR")
                        underlyingXHR.abort()
                    }
                }).then(function(md5) {
                    $(status).html("Processing")
                    console.debug("upload succeeded: md5=" + md5)
                    $(uploadCard).find(".progress").addClass("progress-striped progress-animated")
                    return addCaptureInProcessing(eventId, deviceId, md5)
                }).done(function() {
                    $(uploadCard).remove()
                    console.debug("processing complete")
                }).fail(function() {
                    $(status).html("Upload Failed")
                    console.warn("upload failed")
                })
            })
        })
    }

    function sendMail(to, subject, body) {
        var e = window.encodeURIComponent
        var mailto = "mailto:" + to + "?subject=" + e(subject) + "&body=" + e(body)
        window.location = mailto
    }

    function reportAbuse(capture) {
        var m2 = "funsocialapps.com"
        var url = StitchAPI.eventCaptureDataURL(currentEventId, capture.owner, capture.content_md5)
        var m1 = "abuse"
        var subject = "Report Abuse"
        var m = m1 + "@" + m2
        var body = "Please remove the following inappropriate content.\n\n" + url
        sendMail(m, subject, body)
    }

    function currentPageURL() {
        return document.location.href.split(document.location.search)[0]
    }

    function shareCapture(capture) {
        var e = encodeURIComponent
        var url = currentPageURL() + "?eid=" + e(currentEventId) + "&cmd=" + e(capture.content_md5)
        var content = "photo"
        if (capture.content_type.indexOf("video/") === 0) {
            content = "video"
        }
        var subject = "Check out this " + content + "."
        var body = "Check out this " + content + " from Stitch.\n\n" + url
        sendMail("", subject, body)
    }

    function viewOriginal(capture) {
        var url = StitchAPI.eventCaptureDataURL(currentEventId, capture.owner, capture.content_md5)
        window.open(url)
    }

    function deleteCapture(capture) {
        StitchAPI.deleteCapture(currentEventId, capture.owner, capture.content_md5).done(function() {
            $('.capture[data-md5="' + capture.content_md5 + '"]').remove()
            updateNoPhotosAlert()
        }).fail(function() {
            console.error("Failed to delete capture.")
            addAlert("Failed to delete item.")
        })
    }

    function actionForCapture(capture) {
        $("#action-menu-modal").data("capture", capture).modal("show")
    }

    function registerActionMenuHandlers() {
        function getCap() {
            return $("#action-menu-modal").data("capture")
        }

        $("#report-abuse").on("click", function(e) {
            reportAbuse(getCap())
        })

        $("#share-item").on("click", function(e) {
            shareCapture(getCap())
        })

        $("#view-original").on("click", function(e) {
            viewOriginal(getCap())
        })
        
        $("#delete-item").on("click", function(e) {
            deleteCapture(getCap())
        })

        var modal = $("#action-menu-modal")
        modal.on("show.bs.modal", function(e) {
            var capture = getCap()
            var report = $("#report-abuse")
            var device = deviceIdentity()

            if (device.id !== capture.owner) {
                report.show()
            } else {
                report.hide()
            }

            var deleteItem = $("#delete-item")
            if (StitchAPI.canDeleteCapture(device.id, currentEvent, capture)) {
                deleteItem.show()
            } else {
                deleteItem.hide()
            }
        }).on("click", function(e) {
            modal.modal("hide")
        })
    }

    function shareEvent(event) {
        var url = currentPageURL() + "?event=" + encodeURIComponent(event.id) + "&s=web"
        var subjectTemplate = _.template("Join my \"<%= name %>\" event")
        var subject = subjectTemplate({ name : event.name })

        var bodyTemplate = _.template("View photos/videos from \"<%= name %>\" and add your photos from the event!\n\n<%= url %>")
        var body = bodyTemplate({ name : event.name, url : url })

        sendMail("", subject, body)
    }

    function pairWithATV(eventId) {
        $("#pair-with-atv-modal").modal('show')
    }

    function registerPairWithATVHandlers() {
        $("#pair-with-atv-modal").on('shown.bs.modal', function(e) {
            $("#atv-pairing-code").focus()
        }).on('show.bs.modal', function(e) {
            $("#atv-pairing-code").val("")
        })

        var $modal = $("#pair-with-atv-modal")
        var $form = $modal.find("form")
        var $submit = $form.find(":submit")
        var $input = $("#atv-pairing-code")
        var $danger = $modal.find(".alert-danger")
        var $success = $modal.find(".alert-success")
        var $help = $input.siblings(".text-help")

        $modal.on("show.bs.modal", function(e) {
            $input.val("").parent().removeClass("has-warning")
            $danger.hide()
            $success.hide()
            $submit.prop("disabled", false)
            $submit.html("Subscribe")
            $help.hide()
        }).on("shown.bs.modal", function(e) {
            $input.focus()
        })

        $form.submit(function(e) {
            e.preventDefault()

            $danger.hide(150)

            console.log("subscribing...")
            var code = $input.val()
            if (code.trim().length === 0) {
                console.log("code is 0 length")
                $input.parent().addClass("has-warning")
                $help.show()
                $help.text("Subscription code cannot be blank.")
                return
            }

            $help.hide()
            $input.parent().removeClass("has-warning")

            $submit.prop("disabled", true)
            $submit.html("Subscribing &hellip;")

            StitchAPI.pairWithTV(currentEventId, code).done(function() {
                $submit.html("Subscribed")
                $success.show(150)
                setTimeout(function() {
                    $modal.modal('hide')
                }, 2000)
            }).fail(function() {
                console.error("Failed to subscribe")
                $danger.show(150)
                $submit.prop("disabled", false)
                $submit.html("Subscribe")
            })
        })
    }

    function closestEventIdAttr($e) {
        return $e.closest("[data-eventid]").attr("data-eventid")
    }

    function registerDeleteEventHandlers() {
        var $confirmDelete = $("#confirm-delete-event")
        $("#delete-event").on("click", function(event) {
            $confirmDelete.onConfirm()
            $confirmDelete.modal('hide')
        })

        $(document).on("click", ".delete-event-button", function(event) {
            event.stopPropagation()
            var $target = $(event.target)
            var $item = $target.closest(".event-list-item")
            var id = closestEventIdAttr($target)
            $confirmDelete.onConfirm = function() {
                var list = removeRecentEvent(id)
                $item.hide(150, function() {
                    $item.remove()

                    if (list.length === 0) {
                        // When the load item is deleted, re-run load event list
                        // to get the "create an event" alert
                        loadEventList()
                    }
                })
            }
            $confirmDelete.modal('show')
        })

        $(document).on("click", ".event-list-item", function(event) {
            var id = closestEventIdAttr($(event.target))
            window.location = "/?event=" + id
        })
    }

    function registerEventHandlers() {
        $("#upload-file-input").on("change", function(event) {
            uploadFilesHandler(this.files)
        })

        $(".add-photos-button").on("click", function(event) {
            event.preventDefault()
            $("#upload-file-input").click()
        })

        $(".header img").on("click", function(event) {
            $("#upload-file-input").click()
        })

        $("#set-username").on("click", function(event) {
            promptForUsername().done(function(username) {
                registerDevice()
            })
        })

        $(".create-event-link").on("click", function(event) {
            createEvent()
        })

        $("#share-event").on("click", function(event) {
            shareEvent(currentEvent)
        })

        $("#captures").on("click", ".action", function(event) {
            var capture = $(this).closest('.capture').data('capture')
            actionForCapture(capture)
        })

        $("#pair-with-atv").on("click", function(event) {
            pairWithATV(currentEventId)
        })

        registerCreateEventHandlers()
        registerSetUsernameHandlers()
        registerActionMenuHandlers()
        registerPairWithATVHandlers()
        registerDeleteEventHandlers()
    }

    function setupBranchMetrics(eventId) {
        console.debug("setting up branch metrics with event ID", eventId)
        var branchKey = 'key_live_kndQGOV9IBX4SXqhNFUtSchaAxhZZGse'
        // DO NOT CHECKIN TEST KEY OVERRIDE
        // branchKey = 'key_test_bccVJPGZPv4XT4BkTqQwLfjdyre43IBu'
        branch.init(branchKey)

        if (!has_app) {
            branch.banner({
                icon: 'images/stitch-ios-icon.svg',
                title: 'Stitch',
                description: 'Photo & Video Sharing for Events',
                showiOS: true,
                showAndroid: false,
                showDesktop: false,
                openAppButtonText: 'Open',
                downloadAppButtonText: 'Open',
            }, {
                data: {
                    "event-id": eventId,
                    "$deeplink_path": 'event/' + eventId,
                },
            });
        }

    }

    function iOS() {
        var iDevices = [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ];

        return iDevices.indexOf(navigator.platform) !== -1
    }

    function addAlertNoEscape(title, message, alertClass) {
        var c = alertClass || "alert-danger"
        var t = _.template(
            '<div class="alert <%=alertClass%> alert-dismissible fade in" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '<span class="sr-only">Close</span>' +
                '</button>' +
                '<strong><%=title%></strong> <%=message%>' +
            '</div>')
        var alert = t({title: title, message: message, alertClass: c})
        $("#" + currentViewId).find(".alerts").append(alert)
    }


    function addAlert(title, message, alertClass) {
        addAlertNoEscape(_.escape(title), _.escape(message), alertClass)
    }

    $(document).ready(main)
})(window, document)
