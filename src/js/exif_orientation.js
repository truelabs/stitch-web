// Get a CSS transform from an EXIF Orientation. Requires exif-js and
// the File API.
(function(root) {

    function transformFromOrientation(orientation) {

        /*
           1        2       3      4         5            6           7          8

           888888  888888      88  88      8888888888  88                  88  8888888888
           88          88      88  88      88  88      88  88          88  88      88  88
           8888      8888    8888  8888    88          8888888888  8888888888          88
           88          88      88  88
           88          88  888888  888888
           */

        switch(orientation) {
        case 1:
            return ""
        case 2:
            return "scaleX(-1)"
        case 3:
            return "rotateZ(180deg)"
        case 4:
            return "scaleX(-1) rotateZ(180deg)"
        case 5:
            return "scaleX(-1) rotateZ(90deg)"
        case 6:
            return "rotateZ(90deg)"
        case 7:
            return "scaleX(-1) rotateZ(270deg)"
        case 8:
            return "rotateZ(270deg)"
        }
    }

    function cssTransformFromFile(file) {

        if (file.type.indexOf("image/jpeg") !== 0) {
            return $.Deferred().resolve()
        }

        var defer = $.Deferred()

        var ok = EXIF.getData(file, function() {
            var exif = this
            console.log("EXIF", exif)
            defer.resolve(transformFromOrientation(EXIF.getTag(exif, "Orientation")))
        })

        if (!ok) {
            console.error("EXIF.getData returned false.")
            defer.reject()
        }

        return defer
    }

    root.EXIFOrientation = { "cssTransformFromFile" : cssTransformFromFile }
})(this)
