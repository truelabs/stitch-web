(function(root) {

    var rng;

    var crypto = root.crypto;
    if (crypto && crypto.getRandomValues) {
        var rnds8 = new Uint8Array(16);
        rng = function whatwgRNG() {
            crypto.getRandomValues(rnds8);
            return rnds8;
        };
    } else {
        console.warn("crypto not available, falling back to Math.rng");
        var  rnds = new Array(16);
        rng = function() {
            for (var i = 0, r; i < 16; i++) {
                if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
                rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
            }

            return rnds;
        };
    }

    // hex encoding
    var byteToHex = [];
    var hexToByte = {};
    for (var i = 0; i < 256; i++) {
        byteToHex[i] = (i + 0x100).toString(16).substr(1);
        hexToByte[byteToHex[i]] = i;
    }

    function canonicalString(b) {
        var h = byteToHex;
        return  h[b[0]] + h[b[1]] + h[b[2]] + h[b[3]] + '-' +
            h[b[4]] + h[b[5]] + '-' +
            h[b[6]] + h[b[7]] + '-' +
            h[b[8]] + h[b[9]] + '-' +
            h[b[10]] + h[b[11]] + h[b[12]] + h[b[13]] + h[b[14]] + h[b[15]];
    }

    // RFC 4122 v4 UUID.
    function v4() {
        var r = rng();
        r[6] = (r[6] & 0x0f) | 0x40;
        r[8] = (r[8] & 0x3f) | 0x80;
        return canonicalString(r);
    }

    // API
    var uuid = {};
    uuid.v4 = v4;
    root.uuid = uuid;
})(window);
