(function(root) {
    'use strict'

    var baseURL = "https://stitch-1026.appspot.com"
    var api

    function bestVariantForTargetSize(variants, targetPixels) {
        var best = null
        var bestPixels = 0
        var variantLength = variants.length
        for (var i = 0; i < variantLength; ++i) {
            var variant = variants[i]
            var variantPixels = variant.width * variant.height

            if (best === null) {
                best = variant
                bestPixels = variantPixels
            } else {
                if (Math.abs(variantPixels - targetPixels) < Math.abs(bestPixels - targetPixels)) {
                    best = variant
                    bestPixels = variantPixels
                }
            }
        }

        return best
    }

    function authRequestHeaders() {
        return api.onNeedsCredentials().then(function(deviceId, devicePassword) {
            return {"Device-Id" : deviceId, "Device-Password" : devicePassword}
        })
    }

    function authRequest(settings) {
        return authRequestHeaders().then(function(headers) {
            var merged = {}
            $.extend(merged, {headers: headers}, settings)
            return $.ajax(merged)
        })
    }

    function putEvent(id, name, owner, anyoneCanPost) {
        var data = {
            name: name,
            owner: owner,
            anyone_can_post: anyoneCanPost
        }

        return authRequest({
            url: eventURL(id),
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
        })
    }

    function eventCaptureURL(eventId, deviceId, contentMD5) {
        return baseURL + "/events/" + eventId + "/captures/" + deviceId + "/" + contentMD5
    }

    function eventCaptureDataURL(eventId, deviceId, contentMD5) {
        return eventCaptureURL(eventId, deviceId, contentMD5) + "/data"
    }

    function getUploadURL(eventId, deviceId, contentMD5, contentType, size) {
        return authRequest({
            url: eventCaptureURL(eventId, deviceId, contentMD5) + "/datauploadurl",
            method: "GET",
            data: {"contenttype" : contentType, "size": size},
            dataType: "json",
        }).then(function(json) { return json.url; })
    }

    function putUploadComplete(eventId, deviceId, contentMD5) {
        return authRequest({
            url: eventCaptureURL(eventId, deviceId, contentMD5) + "/uploadcomplete",
            method: "PUT",
        })
    }

    function putFile(file, url, md5Base64) {
        var defer = $.Deferred()
        var xhr = new XMLHttpRequest()

        xhr.upload.addEventListener("progress", function(e) {
            if (e.lengthComputable) {
                defer.notify(e.loaded, e.total, xhr)
            }
        }, false)
        xhr.onload = function() {
            defer.resolve()
        }
        xhr.onabort = function() {
            defer.reject()
        }
        xhr.onerror = function() {
            defer.reject()
        }

        xhr.open("PUT", url)
        xhr.setRequestHeader("Content-MD5", md5Base64)
        xhr.setRequestHeader("Content-Type", file.type)
        xhr.send(file)
        return defer.promise()
    }

    function hexToBase64(str) {
        return btoa(String.fromCharCode.apply(null,
                    str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
                )
    }

    var fileMD5 = root.fileMD5
    function uploadFile(file, eventId, deviceId) {
        console.debug("Uploading " + file.name + ", size " + file.size + ", type " + file.type)
        var theMD5
        return fileMD5(file).then(function(md5) {
            theMD5 = md5
            return getUploadURL(eventId, deviceId, md5, file.type, file.size)
        }).then(function(uploadUrl) {
            var md5Base64 = hexToBase64(theMD5)
            return putFile(file, uploadUrl, md5Base64)
        }).then(function() {
            return putUploadComplete(eventId, deviceId, theMD5)
        }).then(function() {
            return theMD5
        }).promise()
    }

    function deleteCapture(eventId, deviceId, contentMD5) {
        return authRequest({
            url: eventCaptureURL(eventId, deviceId, contentMD5),
            method: "DELETE",
        })
    }

    function registerDevice(deviceId, devicePassword, username) {
        var data = { username: username, password: devicePassword }
        return $.ajax({
            url: baseURL + "/devices/" + deviceId,
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(data),
        })
    }

    function eventURL(eventId) {
        return baseURL + "/events/" + eventId
    }

    function getEvent(eventId) {
        return $.getJSON(eventURL(eventId))
    }

    function isEventOwner(deviceId, event) {
        return deviceId === event.owner
    }

    function canPostToEvent(deviceId, event) {
        return event.anyone_can_post || deviceId == event.owner
    }

    function canDeleteCapture(deviceId, event, capture) {
        return isEventOwner(deviceId, event) || deviceId === capture.owner
    }

    function pairWithTV(eventId, code) {
        return $.ajax({
            url: baseURL + "/pair/codes/" + code + "/value",
            method: "PUT",
            data: eventId,
        })
    }

    api = {
        
        bestThumbnailForWidthAndHeight: function(capture, width, height) {
            return bestVariantForTargetSize(capture.thumbnails, width*height)
        },

        bestVideoForWidthAndHeight: function(capture, width, height) {
            return bestVariantForTargetSize(capture.transcodes, width*height)
        },

        getEvent: getEvent,
        putEvent: putEvent,

        registerDevice: registerDevice,

        // Deferred called with deviceId, devicePassword. Before deferred is done, make sure
        // to registerDevice at least once.
        onNeedsCredentials: function() { console.error("implement me"); return $.Deferred().reject(); },

        uploadFile: uploadFile,
        deleteCapture: deleteCapture,

        eventCaptureDataURL: eventCaptureDataURL,
        isEventOwner: isEventOwner,
        canPostToEvent: canPostToEvent,
        canDeleteCapture: canDeleteCapture,

        pairWithTV: pairWithTV,
    }

    root.StitchAPI = api
})(this)

