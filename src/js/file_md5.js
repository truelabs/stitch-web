(function(global) {
    function md5(file) {
        var defer  = $.Deferred()
        var blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
        chunkSize = 2097152,                             // Read in chunks of 2MB
        chunks = Math.ceil(file.size / chunkSize),
        currentChunk = 0,
        spark = new SparkMD5.ArrayBuffer(),
        fileReader = new FileReader()

        fileReader.onload = function (e) {
            spark.append(e.target.result);                   // Append array buffer
            currentChunk++

            if (currentChunk < chunks) {
                loadNext()
            } else {
                var digest = spark.end()
                console.debug('computed md5', digest);  // Compute hash
                defer.resolve(digest)
            }
        }

        fileReader.onerror = function () {
            console.warn('md5 failed')
            defer.reject()
        }

        function loadNext() {
            var start = currentChunk * chunkSize,
            end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize
            fileReader.readAsArrayBuffer(blobSlice.call(file, start, end))
        }

        loadNext()
        return defer.promise()
    }

    global.fileMD5 = md5
})(this)

