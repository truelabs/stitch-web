package stitchweb

import (
	"github.com/julienschmidt/httprouter"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
	"html/template"
	"net/http"
)

func init() {
	router := &Redirects{wrappedHandler: handlers()}
	http.Handle("/", router)
}

func handlers() http.Handler {
	router := httprouter.New()

	router.GET("/", getIndex)

	return router
}

func getEventID(r *http.Request) string {
	return r.URL.Query().Get("event")
}

type IndexTemplateData struct {
	Eid string
}

func getIndex(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	c := appengine.NewContext(r)
	t, err := template.ParseFiles("static/index.html")
	if err != nil {
		log.Errorf(c, "Failed to parse index template. %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	data := IndexTemplateData{
		Eid: getEventID(r),
	}
	t.Execute(w, data)
}

type Redirects struct {
	wrappedHandler http.Handler
}

func (redirects *Redirects) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Host {
	case "stitchpic.us", "stitch.funsocialapps.com":
		redirectChangingSchemeAndHost(w, r, "http", "get.stitchpic.us")
	case "share.funsocialapps.com":
		redirectChangingHost(w, r, "app.stitchpic.us")
	default:
		redirects.wrappedHandler.ServeHTTP(w, r)
	}
}

func redirectChangingSchemeAndHost(w http.ResponseWriter, r *http.Request, scheme, host string) {
	newUrl := *r.URL
	newUrl.Scheme = scheme
	newUrl.Host = host
	http.Redirect(w, r, newUrl.String(), http.StatusMovedPermanently)
}

func redirectChangingHost(w http.ResponseWriter, r *http.Request, newHost string) {
	redirectChangingSchemeAndHost(w, r, r.URL.Scheme, newHost)
}
