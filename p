#!/bin/bash
# Process JAVASCRIPT and CSS comment commands in HTML files,
# turning them include <script> and <link> tags and sets of
# content versioned files (i.e., files with a SHA of their content
# in the filename). The version files are by definition cache 

set -e

usage() {
    >&2 echo "Error: $1"
    >&2 echo "Usage: p <html_file> <outdir> <debug|prod>"
    exit 1
}

HTML_FILE=$1
[ ! -z "$HTML_FILE" ] || usage "Missing html_file."
shift

OUTDIR=$1
[ ! -z "$OUTDIR" ] || usage "Missing outdir."
shift

MODE=$1
[ ! -z "$MODE" ] || usage "Missing mode."
shift

case $MODE in
    debug)
        generate_javascript=generate_javascript_debug
        generate_css=generate_css_debug
        ;;
    prod)
        generate_javascript=generate_javascript_prod
        generate_css=generate_css_prod
        ;;
    *)
        usage "Invalid mode '$MODE'"
        ;;
esac

declare -i LINENUM=0
export LINENUM

increment_LINENUM() {
    LINENUM=LINENUM+1
}

error() {
    >&2 echo "$LINENUM: error $1"
    exit 1
}

commentCommand() {
    processor=$1
    shift
    [ ! -z "$processor" ] || error "programming error: processor not set"

    local name=""
    local srcdir="." 
    local sources=()
    local attributes=""
    local line
    local gotEND=0
    while IFS=$' \t\n' read -r line; do
        increment_LINENUM
        if [ "$line" == "-->" ]; then
            gotEND=1
            break
        fi
        if [ -z "$line" ]; then
            continue
        fi

        # Everything in between start and end should be key=value
        local key=$(echo -n "$line"|cut -f1 -d=)
        local val=$(echo -n "$line"|cut -f2 -d=)

        case $key in
            name)
                name="$val"
                ;;
            srcdir)
                [[ "$val" =~ ^/ ]] && error "srcdir '$val' is an absolute path."
                srcdir="$val"
                ;;
            src)
                [[ "$val" =~ ^/ ]] && error "src '$val' is an absolute path."
                sources=("${sources[@]}" "$srcdir/$val")
                ;;
            attributes)
                attributes="$val"
                ;;
            *)
                error "Unknown attribute name '$key'."
                ;;
        esac
    done

    if [ $gotEND != 1 ]; then
        error "Expected --> but got EOF."
    fi

    log "Processing $name from sources ${sources[@]}"
    $processor "$name" "$attributes" ${sources[@]}
}

javascript() {
    commentCommand $generate_javascript
}

generate_javascript_debug() {
    mkdir -p "$OUTDIR/js" || error "Failed to create '$OUTDIR'"

    local name=$1
    shift
    local attributes=$1
    shift

    local jsfiles=()

    # Generate the scripts
    local src
    for src in $@; do
        local filename=$(basename "$src")
        local filenameNoExt="${filename%.*}"
        local sha=$(SHA "$src")
        local dst="$filenameNoExt-$sha.js"
        copyFile "$src" "$OUTDIR/js/$dst"
        jsfiles=("${jsfiles[@]}" "$dst")
    done
    
    # Write the HTML tags
    local jsfile
    for jsfile in ${jsfiles[@]}; do
        echo "<script src=\"js/$jsfile\" $attributes></script>"
    done
}

generate_javascript_prod() {
    mkdir -p "$OUTDIR/js" || error "Failed to create '$OUTDIR/js'"

    local name=$1
    shift
    local attributes=$1
    shift

    # Compress and mangle the scripts.
    local tmpjs=$(mktemp /tmp/p-XXXXX)
    minify "$tmpjs" $@
    local sha=$(SHA "$tmpjs")
    local src="js/$name-$sha.js"
    copyFile "$tmpjs" "$OUTDIR/$src"

    echo "<script src=\"$src\" $attributes></script>"
}

css() {
    commentCommand $generate_css
}

generate_css_debug() {
    mkdir -p "$OUTDIR/css" || error "Failed to create '$OUTDIR'"

    local name=$1
    shift
    local attributes=$1
    shift

    local tmpdir=$(mktemp -d /tmp/css-prod-XXXXXX)
    local cssfiles=()

    # Generate the scripts
    local src
    for src in $@; do
        local filename=$(basename "$src")
        local filenameNoExt="${filename%.*}"
        local phase1="$tmpdir/phase1-$i.css"
        runSassc expanded "$src" "$phase1"
        local phase2="$tmpdir/phase2-$i.css"
        autoprefixer "$phase1" "$phase2"
        local sha=$(SHA "$phase2")
        local dst="$filenameNoExt-$sha.css"
        mv "$phase2" "$OUTDIR/css/$dst"
        cssfiles=("${cssfiles[@]}" "$dst")
    done
    
    # Write the HTML tags
    local cssfile
    for cssfile in ${cssfiles[@]}; do
        echo "<link rel=\"stylesheet\" href=\"css/$cssfile\">"
    done
}

generate_css_prod() {
    mkdir -p "$OUTDIR/css" || error "Failed to create '$OUTDIR/js'"

    local name=$1
    shift
    local attributes=$1
    shift

    local tmpdir=$(mktemp -d /tmp/css-prod-XXXXXX)
    local cssfiles=()

    # Convert the SCSS input into CSS
    local -i i=1
    local src
    for src in $@; do
        local dst="$tmpdir/$i.css"
        i=i+1
        local phase1="$tmpdir/phase1-$i.css"
        runSassc compressed "$src" "$phase1"
        autoprefixer "$phase1" "$dst"
        cssfiles=("${cssfiles[@]}" "$dst")
    done

    # Concatenate the CSS
    local tmpcss=$(mktemp /tmp/css-prod-XXXXXX)
    cat "${cssfiles[@]}" > "$tmpcss"
    local sha=$(SHA "$tmpcss")
    local combined="css/$name-$sha.css"
    mv "$tmpcss" "$OUTDIR/$combined"

    echo "<link rel=\"stylesheet\" href=\"$combined\">"
}

SHA() {
    log shasum $1
    shasum -a 256 "$1"|cut -f1 -d ' '|cut -c 1-8
}

autoprefixer() {
    log autoprefixer "$@"
    postcss --use autoprefixer --output "$2" "$1"
}

copyFile() {
    log cp $@
    cp $@
}

runSassc() {
    log sassc $@
    sassc --style "$1" "$2" "$3"
}

minify() {
    log minify $@
    local output=$1
    shift
    uglifyjs $@ -c -m -o "$output"
}

exec 3>&1
log() {
    echo "$(date +"%H:%M:%S") p" $@ >&3
}

main() {
    local name
    local line
    local htmlout="$OUTDIR/$(basename "$HTML_FILE")"
    while IFS="" read -r line; do
        increment_LINENUM
        fields=$(echo "$line"|tr -s '[:space:]' ' '|sed 's/^ *//'|cut -f1 -d " ")
        case "$fields" in
            "<!--JAVASCRIPT")
                log "JAVASCRIPT command comment"
                javascript
                ;;
            "<!--CSS")
                log "CSS command comment"
                css
                ;;
            *)
                echo "$line"
        esac
    done < $HTML_FILE > $htmlout
}

main
