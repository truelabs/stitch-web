# stitch-web
Web client for Stitch.

## Building the Static Web Site
Setup a development environment by running `./web setup`.

Build by running `./web build`.

Serve for development by running `./web serve debug`.

To deploy, `./web deploy`.

## Directories

    /src
        /js application JavaScript
        /css application SASC, which is turned into CSS at build time
        /html application HTML. Processed by the `p` command.
    /images application images
    /js-lib 3rd party JavaScript libraries.
